require 'spec_helper'

describe 'istartupops::profile::bastion' do
  context 'supported operating systems' do

    default_facts = { 'is_virtual' => 'false', 'concat_basedir' => '/tmp/concat' }

    os_facts = { 
      'RedHat6' => { 'osfamily' => 'RedHat', 'operatingsystem' => 'RedHat', 'operatingsystemmajrelease' => '6' },
      'RedHat7' => { 'osfamily' => 'RedHat', 'operatingsystem' => 'RedHat', 'operatingsystemmajrelease' => '7' },
      'CentOS6' => { 'osfamily' => 'RedHat', 'operatingsystem' => 'CentOS', 'operatingsystemmajrelease' => '6' },
      'CentOS7' => { 'osfamily' => 'RedHat', 'operatingsystem' => 'CentOS', 'operatingsystemmajrelease' => '7' },
      'Ubuntu1404' => { 'osfamily' => 'Debian', 'operatingsystem' => 'Ubuntu', 'operatingsystemmajrelease' => '14' }
    }

    os_facts.each do |os, facts|
      context "on #{os}" do
        let(:facts) do
          facts.merge!(default_facts)
        end

        context "istartup::profile::bastion with defaults" do
          describe "without any parameters" do
            let(:params) {{ 'admin_password_hash' => 'blahblahblah' }}

            # basic sanity checks
            it { is_expected.to compile.with_all_deps }

            it { is_expected.to contain_class('istartupops::params') }
            it { is_expected.to contain_class('istartupops') }
            it { is_expected.to contain_class('istartupops::profile') }
            it { is_expected.to contain_class('istartupops::profile::bastion') }
          end
        end

      end
    end
  end

  context 'unsupported operating system' do
    describe 'istartupops::profile::bastion class on Solaris/Nexenta' do
      let(:facts) {{ :osfamily => 'Solaris', :operatingsystem => 'Nexenta', }}
      let(:params) {{ 'admin_password_hash' => 'blahblah' }}

      it { expect { catalogue }.to raise_error(Puppet::Error, /not supported/) }
    end
  end
end
