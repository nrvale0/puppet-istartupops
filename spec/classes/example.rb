require 'spec_helper'

describe 'istartupops' do
  context 'supported operating systems' do
    on_supported_os.each do |os, facts|
      context "on #{os}" do
        let(:facts) do
          facts
        end

        context "istartupops class without any parameters" do
          let(:params) {{ }}

          it { is_expected.to compile.with_all_deps }

          it { is_expected.to contain_class('istartupops::params') }
          it { is_expected.to contain_class('istartupops::install').that_comes_before('istartupops::config') }
          it { is_expected.to contain_class('istartupops::config') }
          it { is_expected.to contain_class('istartupops::service').that_subscribes_to('istartupops::config') }

          it { is_expected.to contain_service('istartupops') }
          it { is_expected.to contain_package('istartupops').with_ensure('present') }
        end
      end
    end
  end

  context 'unsupported operating system' do
    describe 'istartupops class without any parameters on Solaris/Nexenta' do
      let(:facts) {{
        :osfamily        => 'Solaris',
        :operatingsystem => 'Nexenta',
      }}

      it { expect { is_expected.to contain_package('istartupops') }.to raise_error(Puppet::Error, /Nexenta not supported/) }
    end
  end
end
