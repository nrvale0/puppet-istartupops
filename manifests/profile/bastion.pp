class istartupops::profile::bastion(
  $admin_password_hash,
  $admin_user = 'admin',
  $ssh_authorized_keys = {},
) inherits ::istartupops::params {

  require ::istartupops::profile

  validate_re($admin_password_hash, '^.+$')
  validate_re($admin_user, '^.+$')
  validate_hash($ssh_authorized_keys)

  user { $admin_user: 
    ensure => present,
    password => $admin_password_hash,
    managehome => true,
  }

  $ssh_authorized_keys_defaults = { user => $admin_user, }
  create_resources('ssh_authorized_key', $ssh_authorized_keys, $ssh_authorized_keys_defaults)

  class { '::ssh': }
}
